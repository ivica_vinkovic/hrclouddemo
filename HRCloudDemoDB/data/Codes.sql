﻿
CREATE TABLE #Source ([Id] int, [Type] nvarchar(50));

  INSERT INTO #Source ([Id],[Type])
     VALUES (1,'Phone')

  INSERT INTO #Source ([Id],[Type])
     VALUES (2,'Mobile')

  INSERT INTO #Source ([Id],[Type])
     VALUES (3,'Email')

  INSERT INTO #Source ([Id],[Type])
     VALUES (4,'Fax')

  INSERT INTO #Source ([Id],[Type])
     VALUES (5,'Skype')


SET Identity_insert [dbo].[ConnectionType] ON


MERGE [dbo].[ConnectionType] AS T
USING #Source AS S
ON (T.[id] = S.[id]) 
WHEN NOT MATCHED BY TARGET
    THEN INSERT([Id], [Type]) VALUES(S.[Id], S.[Type])
WHEN MATCHED 
    THEN UPDATE SET T.[Type] = S.[Type]
WHEN NOT MATCHED BY SOURCE
	THEN DELETE;


SET Identity_insert [dbo].[ConnectionType] OFF


DROP TABLE #Source
