﻿CREATE TABLE [dbo].[Connection] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [TypeID]    INT           NULL,
    [Value]     NVARCHAR (50) NULL,
    [ContactID] INT           NULL,
    CONSTRAINT [PK__Connecti__3214EC077369C085] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Connection_ConnectionType] FOREIGN KEY ([TypeID]) REFERENCES [dbo].[ConnectionType] ([Id]),
    CONSTRAINT [FK_Connection_Contact] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contact] ([Id])
);





