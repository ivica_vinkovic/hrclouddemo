﻿CREATE TABLE [dbo].[ContactTags] (
    [Id]        INT IDENTITY (1, 1) NOT NULL,
    [ContactID] INT NULL,
    [TagID]     INT NULL,
    CONSTRAINT [PK_ContactTags] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ContactTags_Contact] FOREIGN KEY ([ContactId]) REFERENCES [dbo].[Contact] ([Id]),
    CONSTRAINT [FK_ContactTags_Tags] FOREIGN KEY ([TagId]) REFERENCES [dbo].[Tags] ([Id])
);

