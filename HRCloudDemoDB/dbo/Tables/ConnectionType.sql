﻿CREATE TABLE [dbo].[ConnectionType] (
    [Id]   INT           IDENTITY (1, 1) NOT NULL,
    [Type] NVARCHAR (50) NULL,
    CONSTRAINT [PK__Connecti__3214EC07735BAB98] PRIMARY KEY CLUSTERED ([Id] ASC)
);



