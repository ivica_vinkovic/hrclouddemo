﻿CREATE TABLE [dbo].[Contact] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50) NULL,
    [Surname]     NVARCHAR (50) NULL,
    [Title]       NVARCHAR (50) NULL,
    [MiddleName]  NVARCHAR (50) NULL,
    [DateOfBirth] DATE          NULL,
    CONSTRAINT [PK__tmp_ms_x__3214EC07581EDB7A] PRIMARY KEY CLUSTERED ([Id] ASC)
);





