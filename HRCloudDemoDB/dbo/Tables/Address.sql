﻿CREATE TABLE [dbo].[Address] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [Street]    NVARCHAR (50) NULL,
    [No]        NVARCHAR (50) NULL,
    [ZipCode]   NVARCHAR (50) NULL,
    [City]      NVARCHAR (50) NULL,
    [County]    NVARCHAR (50) NULL,
    [Country]   NVARCHAR (50) NULL,
    [ContactID] INT           NULL,
    CONSTRAINT [PK__Address__3214EC07887C3802] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Address_Contact] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contact] ([Id])
);







