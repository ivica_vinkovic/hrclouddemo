var AngularHRCloud = angular.module('AngularHRCloud', [
  'ngRoute',
  'ContactModule',
  'ui.bootstrap'
]);

AngularHRCloud.config(['$routeProvider',
  function ($routeProvider) {
      var templatePrefix = 'Template/Scripts/';
      $routeProvider.
        when('/contacts', {
            templateUrl: templatePrefix + 'Contact/ContactList.html',
            controller: 'ContactListCtrl'
        }).
        when('/contacts-new', {
            templateUrl: templatePrefix + 'Contact/Contact.html',
            controller: 'ContactCtrl'
        }).
        when('/contacts-edit/:id', {
            templateUrl: templatePrefix + 'Contact/Contact.html',
            controller: 'ContactCtrl'
        }).
        otherwise({
            redirectTo: '/contacts'
        });
  }]);
var ContactModule = angular.module('ContactModule', []);


ContactModule.controller('ContactListCtrl', [
    '$scope', '$http', '$uibModal', '$location', function ($scope, $http, $uibModal, $location) {
        var templatePrefix = 'Template/Scripts/';
        $scope.SearchText = '';

        $scope.$watch('[SearchText]', function(v) {
            $scope.search();
        });

        $scope.search = function() {
            $http.post('api/Contact', '"' + $scope.SearchText + '"').success(function(data) {
                $scope.Contacts = data.Contacts;
            });
        };

        $scope.go = function (path) {
            $location.path(path);
        };

        $scope.delete = function (contact) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: templatePrefix + 'Contact/ConfirmationModal.html',
                controller: 'ConfirmationModalCtrl',
                size: 'sm'
            });

            modalInstance.result.then(function(isTrue) {
                if (isTrue) {
                    var config = {
                        method: "DELETE",
                        url: 'api/Contact',
                        data: contact,
                        headers: { "Content-Type": "application/json;charset=utf-8" }
                    };
                    $http(config).success(function (data) {
                        $http.post('api/Contact', '"' + $scope.SearchText + '"').success(function (data) {
                            $scope.Contacts = data.Contacts;
                        });
                    });
                }
            });
        };

    }
]);

ContactModule.controller('ContactCtrl', [
    '$scope', '$http', '$uibModal', '$routeParams', '$location', function ($scope, $http, $uibModal, $routeParams, $location) {

        var templatePrefix = 'Template/Scripts/';

        $scope.title = $routeParams.id == undefined ? "New" : "Edit";
        $http.get('api/Contact/'+ ($routeParams.id == undefined ? 0 : $routeParams.id)).success(function (data) {
            $scope.Contact = data;
        });

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[2];


        $scope.openCalendar = function ($event) {
            $scope.status.opened = true;
        };

        $scope.status = {
            opened: false
        };

        $scope.go = function (path) {
            $location.path(path);
        };

        $scope.type = function (type) {
            var retValue = "";
            switch (type) {
                case 1:
                    retValue = "Phone";
                    break;
                case 2:
                    retValue = "Mobile";
                    break;
                case 3:
                    retValue = "Email";
                    break;
                case 4:
                    retValue = "Fax";
                    break;
                case 5:
                    retValue = "Skype";
                    break;
                default:
                    retValue = "Phone";
                    break;
            }
            return retValue;
        }

        $scope.addAddress = function () {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: templatePrefix + 'Contact/AddressModal.html',
                controller: 'AddressModalCtrl',
                size: 'lg'
            });

            modalInstance.result.then(function (newAddress) {
                $scope.Contact.Address.push(newAddress);
            });
        };

        $scope.addConnection = function () {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: templatePrefix + 'Contact/ConnectionModal.html',
                controller: 'ConnectionModalCtrl',
                size: 'lg'
            });

            modalInstance.result.then(function (newConnection) {
                $scope.Contact.Connections.push(newConnection);
            });
        };

        $scope.save = function() {
            $http.patch('api/Contact', $scope.Contact).success(function (data) {
                $scope.Contact = data;
            });
        }

        $scope.deleteAddress = function (address) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: templatePrefix + 'Contact/ConfirmationModal.html',
                controller: 'ConfirmationModalCtrl',
                size: 'sm'
            });

            modalInstance.result.then(function (isTrue) {
                if (isTrue) {
                    address.IsDeleted = true;
                }
            });
        };

        $scope.deleteConnection = function (connection) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: templatePrefix + 'Contact/ConfirmationModal.html',
                controller: 'ConfirmationModalCtrl',
                size: 'sm'
            });

            modalInstance.result.then(function (isTrue) {
                if (isTrue) {
                    connection.IsDeleted = true;
                }
            });
        };

    }
]);


ContactModule.controller('AddressModalCtrl', ['$scope', '$http', '$uibModalInstance', function ($scope, $http, $uibModalInstance) {
    $scope.newAddress = {
        Street: "",
        No: "",
        ZipCode: "",
        City: "",
        County: "",
        Country: ""
    }

    $scope.ok = function () {
        $uibModalInstance.close($scope.newAddress);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

ContactModule.controller('ConnectionModalCtrl', ['$scope', '$http', '$uibModalInstance', function ($scope, $http, $uibModalInstance) {
    $scope.newConnection = {
        TypeID: "",
        Value: ""
    }

    $scope.types = [
        { type: 1, value: "Phone" },
        { type: 2, value: "Mobile" },
        { type: 3, value: "Email" },
        { type: 4, value: "Fax" },
        { type: 5, value: "Skype" }
    ];

    $scope.ok = function () {
        $uibModalInstance.close($scope.newConnection);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

ContactModule.controller('ConfirmationModalCtrl', ['$scope', '$http', '$uibModalInstance', function ($scope, $http, $uibModalInstance) {


    $scope.ok = function () {
        $uibModalInstance.close(true);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

ContactModule.directive('tagManager', function ($http) {
    return {
        restrict: 'E',
        scope: { tags: '=' },
        template:
            '<div class="tags">' +
                '<a ng-repeat="tag in tags" class="tag" ng-click="remove(tag)" ng-hide="tag.IsDeleted">{{tag.Name}}</a>' +
                '</div>' +
                '<div class="col-lg-4"><div class="input-group">' +
                '<input type="text" ng-model="selected" uib-typeahead="selectTag.Value for selectTag in selectTags | filter:{Value:$viewValue} | limitTo:8" class="form-control">' +
                '<span class="input-group-btn">' +
                '<button type="button" class="btn btn-default" ng-click="add()">Add tag</button>' +
                '</span>' +
                '</div></div>',
        link: function ($scope, $element) {

            var input = angular.element($element.children()[1]);

            $scope.selected = undefined;
            $http.get('api/Tag').success(function (data) {
                $scope.selectTags = data.Tag;
            });

            var tag = {Tag:[{Value:"",IsDeleted:false}]}
            // This adds the new tag to the tags array
            $scope.add = function () {
                if ($scope.selected !== "") {
                    var arr = $.grep($scope.selectTags, function(a) {
                        return a.Value === $scope.selected;
                    });
                    if (arr.length > 0) {
                        $scope.tags.push({ TagID: arr[0].Id, Name: arr[0].Value, IsDeleted: false });
                    } else {
                        tag.Tag[0].Value = $scope.selected;
                        $http.patch('api/Tag', tag).success(function(data) {
                            $scope.tags.push({ TagID: data.Tag[0].Id, Name: tag.Tag[0].Value, IsDeleted: false });
                        });
                    }
                }
                $scope.selected = undefined;
            };

            // This is the ng-click handler to remove an item
            $scope.remove = function (tag) {
                tag.IsDeleted = true;
            };

            // Capture all keypresses
            input.bind('keypress', function (event) {
                // But we only care when Enter was pressed
                if (event.keyCode == 13) {
                    // There's probably a better way to handle this...
                    $scope.$apply($scope.add);
                }
            });
        }
    };
});

angular.module('AngularHRCloud').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('Template/Scripts/Contact/AddressModal.html',
    "<div class=\"modal-header\"><h3 class=\"modal-title\">Addresses</h3></div><div class=\"modal-body\"><div><div class=\"row\"><div class=\"col-md-3\">Street:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"newAddress.Street\" class=\"form-control\" placeholder=\"Street\"></div><div class=\"col-md-3\">No:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"newAddress.No\" class=\"form-control\" placeholder=\"No\"></div></div><div class=\"row\"><div class=\"col-md-3\">ZipCode:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"newAddress.ZipCode\" class=\"form-control\" placeholder=\"ZipCode\"></div><div class=\"col-md-3\">City:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"newAddress.City\" class=\"form-control\" placeholder=\"City\"></div></div><div class=\"row\"><div class=\"col-md-3\">County:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"newAddress.County\" class=\"form-control\" placeholder=\"County\"></div><div class=\"col-md-3\">Country:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"newAddress.Country\" class=\"form-control\" placeholder=\"Country\"></div></div></div></div><div class=\"modal-footer\"><button class=\"btn btn-primary\" type=\"button\" ng-click=\"ok()\">OK</button> <button class=\"btn btn-warning\" type=\"button\" ng-click=\"cancel()\">Cancel</button></div>"
  );


  $templateCache.put('Template/Scripts/Contact/ConfirmationModal.html',
    "<div class=\"modal-header\"><h3 class=\"modal-title\">Delete!</h3></div><div class=\"modal-body\">Are you sure?</div><div class=\"modal-footer\"><button class=\"btn btn-primary\" type=\"button\" ng-click=\"ok()\">OK</button> <button class=\"btn btn-warning\" type=\"button\" ng-click=\"cancel()\">Cancel</button></div>"
  );


  $templateCache.put('Template/Scripts/Contact/ConnectionModal.html',
    "<div class=\"modal-header\"><h3 class=\"modal-title\">Connections</h3></div><div class=\"modal-body\"><div><div class=\"row\"><div class=\"col-md-3\"><select name=\"select\" id=\"select\" ng-model=\"newConnection.TypeID\" class=\"form-control\"><option ng-repeat=\"type in types\" value=\"{{type.type}}\">{{type.value}}</option></select></div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"newConnection.Value\" class=\"form-control\" placeholder=\"Value\"></div><div class=\"col-md-3\"></div><div class=\"col-md-3\"></div></div></div></div><div class=\"modal-footer\"><button class=\"btn btn-primary\" type=\"button\" ng-click=\"ok()\">OK</button> <button class=\"btn btn-warning\" type=\"button\" ng-click=\"cancel()\">Cancel</button></div>"
  );


  $templateCache.put('Template/Scripts/Contact/Contact.html',
    "<div class=\"container\"><h3>Contact - {{title}}</h3><br><br><div class=\"row\"><div class=\"col-md-10\">Click on tag to delete it!<br><div class=\"tags\"><tag-manager tags=\"Contact.Tags\"></tag-manager></div></div><div class=\"col-md-2\"><button type=\"button\" class=\"btn btn-primary\" ng-click=\"save()\">Save</button> <button type=\"button\" class=\"btn btn-danger\" ng-click=\"go('/contacts')\">Cancel</button></div></div><br><div class=\"row\"><div class=\"col-md-3\">Name:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"Contact.Contact.Name\" class=\"form-control\" placeholder=\"Name\"></div><div class=\"col-md-3\">Title:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"Contact.Contact.Title\" class=\"form-control\" placeholder=\"Title\"></div></div><div class=\"row\"><div class=\"col-md-3\">Surname:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"Contact.Contact.Surname\" class=\"form-control\" placeholder=\"Surname\"></div><div class=\"col-md-3\">Middlename:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"Contact.Contact.MiddleName\" class=\"form-control\" placeholder=\"Middlename\"></div></div><div class=\"row\"><div class=\"col-md-3\">Date of Birth:</div><div class=\"col-md-3\"><p class=\"input-group\"><input type=\"text\" class=\"form-control\" uib-datepicker-popup=\"{{format}}\" ng-model=\"Contact.Contact.DateOfBirth\" is-open=\"status.opened\" min-date=\"minDate\" max-date=\"maxDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\" ng-required=\"true\" close-text=\"Close\"> <span class=\"input-group-btn\"><button type=\"button\" class=\"btn btn-default\" ng-click=\"openCalendar($event)\"><i class=\"glyphicon glyphicon-calendar\"></i></button></span></p></div><div class=\"col-md-3\"></div><div class=\"col-md-3\"></div></div><br><h4>Addresses</h4><br><button type=\"button\" class=\"btn btn-primary\" ng-click=\"addAddress()\">Add new address</button><br><br><div ng-repeat=\"address in Contact.Address\" ng-hide=\"address.IsDeleted\"><div class=\"row\"><div class=\"col-md-3\">Street:</div><div class=\"col-md-6\" style=\"padding: 0px\"><div class=\"col-md-9\"><input type=\"text\" ng-model=\"address.Street\" class=\"form-control\" placeholder=\"Street\"></div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"address.No\" class=\"form-control\" placeholder=\"No\"></div></div><div class=\"col-md-3\"><button type=\"button\" class=\"btn btn-danger\" ng-click=\"deleteAddress(address)\">X</button></div></div><div class=\"row\"><div class=\"col-md-3\">ZipCode:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"address.ZipCode\" class=\"form-control\" placeholder=\"ZipCode\"></div><div class=\"col-md-3\">City:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"address.City\" class=\"form-control\" placeholder=\"City\"></div></div><div class=\"row\"><div class=\"col-md-3\">County:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"address.County\" class=\"form-control\" placeholder=\"County\"></div><div class=\"col-md-3\">Country:</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"address.Country\" class=\"form-control\" placeholder=\"Country\"></div></div></div><br><br><h4>Connections</h4><br><button type=\"button\" class=\"btn btn-primary\" ng-click=\"addConnection()\">Add new contact</button><br><br><div ng-repeat=\"connection in Contact.Connections\" ng-hide=\"connection.IsDeleted\"><div class=\"row\"><div class=\"col-md-3\">{{type(connection.TypeID)}}</div><div class=\"col-md-3\"><input type=\"text\" ng-model=\"connection.Value\" class=\"form-control\" placeholder=\"Value\"></div><div class=\"col-md-3\"><button type=\"button\" class=\"btn btn-danger\" ng-click=\"deleteConnection(connection)\">X</button></div><div class=\"col-md-3\"></div></div></div></div>"
  );


  $templateCache.put('Template/Scripts/Contact/ContactList.html',
    "<div class=\"container\"><h3>Contacts</h3><p><div class=\"search-bar\"><input type=\"text\" ng-model=\"SearchText\" class=\"form-control\" ng-model-options=\"{ debounce: global.debounce }\" placeholder=\"Search contact\"></div></p><table class=\"table\"><thead><tr><th>Firstname</th><th>Lastname</th><th>Date of birth</th><th>Tags</th><th><button type=\"button\" class=\"btn btn-primary\" ng-click=\"go('/contacts-new')\">Add new contact</button></th></tr></thead><tbody><tr ng-repeat=\"contact in Contacts\"><td ng-bind=\"contact.Contact.Name\"></td><td ng-bind=\"contact.Contact.Surname\"></td><td ng-bind=\"contact.Contact.DateOfBirth | date:'dd.MM.yyyy'\"></td><td><div class=\"tags\"><a ng-repeat=\"tag in contact.Tags\" class=\"tag\">{{tag.Name}}</a></div></td><td><div class=\"tags\"><button type=\"button\" class=\"btn btn-primary\" ng-click=\"go('/contacts-edit/'+contact.Contact.Id)\">Edit</button> <button type=\"button\" class=\"btn btn-danger\" ng-click=\"delete(contact.Contact)\">Delete</button></div></td></tr></tbody></table></div>"
  );

}]);
