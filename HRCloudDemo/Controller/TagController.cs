﻿using System;
using System.Web.Http;
using HRCloudBusinessLayer.DTO.Mapper;

namespace HRCloudDemo.Controller
{
    public class TagController : ApiController
    {
        public HRCloudBusinessLayer.Entities.TagsEntity Get()
        {
            var tags = new HRCloudBusinessLayer.Entities.TagsEntity();
            tags.Fill();
            return tags;
        }

        [HttpDelete]
        public IHttpActionResult Delete(HRCloudBusinessLayer.DTO.Tags tags)
        {
            var tagsMapper = new TagsMapper(tags);
            try
            {
                tagsMapper.DeleteFromDatabase();
                return Ok(true);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPatch]
        public IHttpActionResult Update(HRCloudBusinessLayer.Entities.TagsEntity tags)
        {
            try
            {
                tags.UpSert();
                return Ok(tags);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
