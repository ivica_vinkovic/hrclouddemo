﻿using System;
using System.Web.Http;
using HRCloudBusinessLayer.DTO;
using HRCloudBusinessLayer.DTO.Mapper;
using HRCloudBusinessLayer.Entities;

namespace HRCloudDemo.Controller
{
    public class ContactController : ApiController
    {
        public IHttpActionResult Get(int id)
        {
            var contact = new ContactEntity(id);
            return Ok(contact);
        }

        [HttpPost]
        public IHttpActionResult Search([FromBody] string value)
        {
            var contacts = new ContactCollection(value); 
            return Ok(contacts);
        }

        [HttpDelete]
        public IHttpActionResult Delete(Contact contact)
        {
            var contactMapper = new ContactMapper(contact);
            try
            {
                contactMapper.DeleteFromDatabase();
                return Ok(true);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPatch]
        public IHttpActionResult Update(ContactEntity contactEntity)
        {
            try
            {
                contactEntity.UpSert();
                return Ok(contactEntity);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}