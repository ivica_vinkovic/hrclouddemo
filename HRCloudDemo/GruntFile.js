﻿/// <reference path="bower_components/jquery/dist/jquery.js" />
module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        ngtemplates: {
            app: {
                src: 'Scripts/**/*.html',
                dest: 'JsBin/Templates.js',
                options: {
                    module: 'AngularHRCloud',
                    htmlmin: {
                        collapseWhitespace: true,
                        collapseBooleanAttributes: true
                    },
                    prefix: 'Template/'
                }
            }
        },

        env: {
            options: {
                /* Shared Options Hash */
                //globalOption : 'foo'
            },
            dev: {
                NODE_ENV: 'DEVELOPMENT'
            },
            prod: {
                NODE_ENV: 'PRODUCTION'
            }
        },
        preprocess: {
            dev: {
                src: 'index-tmpl.html',
                dest: 'index.html'
            },
            prod: {
                src: 'index-tmpl.html',
                dest: 'index.html',
                options: {
                    context: {
                        name: 'Ivica',
                        version: 'v1',
                        now: 'v1',
                        ver: 'v1'
                    }
                }
            }
        },
        concat: {
            js: {
                src: [
                'Scripts/App.js',
                'Scripts/*/*.js',
                'JsBin/Templates.js'
                ],
                dest: 'Release/Scripts/HRCloudDemo.js'
            },
            css: {
                src: ['bower_components/bootstrap/dist/css/bootstrap.css',
                        'bower_components/bootstrap/dist/css/bootstrap-theme.css',
                        'Css/**/*.css'],
                dest: 'Release/Css/HRCloudDemo.css'
            },
            libs: {
                src: ['bower_components/jquery/dist/jquery.js',
                        'bower_components/angular/angular.js',
                        'bower_components/angular-route/angular-route.js',
                        'bower_components/bootstrap/dist/js/bootstrap.js',
                        'bower_components/angular-bootstrap/ui-bootstrap.js',
                        'bower_components/angular-bootstrap/ui-bootstrap-tpls.js'],
                dest: 'Release/Libs/Libraries.js'
            }
        },
        uglify: {
            build: {
                src: 'Release/Scripts/HRCloudDemo.js',
                dest: 'Release/Scripts/HRCloudDemo.min.js'
            },
            libs: {
                src: 'Release/Libs/Libraries.js',
                dest: 'Release/Libs/Libraries.min.js'
            }
        },
        cssmin: {
            compress: {
                files: {
                    'Release/Css/HRCloudDemo.min.css': ['Release/Css/HRCloudDemo.css']
                }
            }
        },
        watch:
        {
            scripts: {
                files: ['Scripts/**/*.js', 'Css/**/*.css','Scripts/**/*.html'],
                tasks: ['ngtemplates', 'concat', 'env:dev', 'preprocess:dev']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-preprocess');
    grunt.loadNpmTasks('grunt-angular-templates');

    // Default task(s).
    grunt.registerTask('default', ['ngtemplates', 'concat', 'env:dev', 'preprocess:dev', 'watch']);

    grunt.registerTask('dev', ['ngtemplates', 'concat', 'env:dev', 'preprocess:dev']);

    grunt.registerTask('prod', ['ngtemplates', 'concat', 'env:prod', 'uglify', 'cssmin', 'preprocess:prod']);

};