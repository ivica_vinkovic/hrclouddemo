﻿
ContactModule.directive('tagManager', function($http) {
    return {
        restrict: 'E',
        scope: { tags: '=' },
        template:
            '<div class="tags">' +
                '<a ng-repeat="tag in tags" class="tag" ng-click="remove(tag)" ng-hide="tag.IsDeleted">{{tag.Name}}</a>' +
                '</div>' +
                '<div class="col-lg-4"><div class="input-group">' +
                '<input type="text" ng-model="selected" uib-typeahead="selectTag.Value for selectTag in selectTags | filter:{Value:$viewValue} | limitTo:8" class="form-control">' +
                '<span class="input-group-btn">' +
                '<button type="button" class="btn btn-default" ng-click="add()">Add tag</button>' +
                '</span>' +
                '</div></div>',
        link: function($scope, $element) {

            var input = angular.element($element.children()[1]);

            $scope.selected = undefined;
            $http.get('api/Tag').success(function(data) {
                $scope.selectTags = data.Tag;
            });

            var tag = { Tag: [{ Value: "", IsDeleted: false }] }
            // This adds the new tag to the tags array
            $scope.add = function() {
                if ($scope.selected !== "") {
                    var arr = $.grep($scope.selectTags, function(a) {
                        return a.Value === $scope.selected;
                    });
                    if (arr.length > 0) {
                        $scope.tags.push({ TagID: arr[0].Id, Name: arr[0].Value, IsDeleted: false });
                    } else {
                        tag.Tag[0].Value = $scope.selected;
                        $http.patch('api/Tag', tag).success(function(data) {
                            $scope.tags.push({ TagID: data.Tag[0].Id, Name: tag.Tag[0].Value, IsDeleted: false });
                        });
                    }
                }
                $scope.selected = undefined;
            };

            // This is the ng-click handler to remove an item
            $scope.remove = function(tag) {
                tag.IsDeleted = true;
            };

            // Capture all keypresses
            input.bind('keypress', function(event) {
                // But we only care when Enter was pressed
                if (event.keyCode == 13) {
                    // There's probably a better way to handle this...
                    $scope.$apply($scope.add);
                }
            });
        }
    };
});
