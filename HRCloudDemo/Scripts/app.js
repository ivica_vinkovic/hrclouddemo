﻿var AngularHRCloud = angular.module('AngularHRCloud', [
  'ngRoute',
  'ContactModule',
  'ui.bootstrap'
]);
var config = AngularHRCloud.config(['$routeProvider',
    function ($routeProvider) {
        var templatePrefix = 'Template/Scripts/';
        $routeProvider.
            when('/contacts', {
                templateUrl: templatePrefix + 'Contact/ContactList.html',
                controller: 'ContactListCtrl'
            }).
            when('/contacts-new', {
                templateUrl: templatePrefix + 'Contact/Contact.html',
                controller: 'ContactCtrl'
            }).
            when('/contacts-edit/:id', {
                templateUrl: templatePrefix + 'Contact/Contact.html',
                controller: 'ContactCtrl'
            }).
            otherwise({
                redirectTo: '/contacts'
            });
    }]);