﻿ContactModule.controller('AddressModalCtrl', [
    '$scope', '$http', '$uibModalInstance', function($scope, $http, $uibModalInstance) {
        $scope.newAddress = {
            Street: "",
            No: "",
            ZipCode: "",
            City: "",
            County: "",
            Country: ""
        }

        $scope.ok = function() {
            $uibModalInstance.close($scope.newAddress);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);

ContactModule.controller('ConnectionModalCtrl', [
    '$scope', '$http', '$uibModalInstance', function($scope, $http, $uibModalInstance) {
        $scope.newConnection = {
            TypeID: "",
            Value: ""
        }

        $scope.types = [
            { type: 1, value: "Phone" },
            { type: 2, value: "Mobile" },
            { type: 3, value: "Email" },
            { type: 4, value: "Fax" },
            { type: 5, value: "Skype" }
        ];

        $scope.ok = function() {
            $uibModalInstance.close($scope.newConnection);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);

ContactModule.controller('ConfirmationModalCtrl', [
    '$scope', '$http', '$uibModalInstance', function($scope, $http, $uibModalInstance) {


        $scope.ok = function() {
            $uibModalInstance.close(true);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);