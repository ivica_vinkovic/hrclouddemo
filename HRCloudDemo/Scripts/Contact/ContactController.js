﻿var ContactModule = angular.module('ContactModule', []);


ContactModule.controller('ContactListCtrl', [
    '$scope', '$http', '$uibModal', '$location', function($scope, $http, $uibModal, $location) {
        var templatePrefix = 'Template/Scripts/';
        $scope.SearchText = '';

        $scope.$watch('[SearchText]', function(v) {
            $scope.search();
        });

        $scope.search = function() {
            $http.post('api/Contact', '"' + $scope.SearchText + '"').success(function(data) {
                $scope.Contacts = data.Contacts;
            });
        };

        $scope.go = function(path) {
            $location.path(path);
        };

        $scope.delete = function(contact) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: templatePrefix + 'Contact/ConfirmationModal.html',
                controller: 'ConfirmationModalCtrl',
                size: 'sm'
            });

            modalInstance.result.then(function(isTrue) {
                if (isTrue) {
                    var config = {
                        method: "DELETE",
                        url: 'api/Contact',
                        data: contact,
                        headers: { "Content-Type": "application/json;charset=utf-8" }
                    };
                    $http(config).success(function(data) {
                        $http.post('api/Contact', '"' + $scope.SearchText + '"').success(function(data) {
                            $scope.Contacts = data.Contacts;
                        });
                    });
                }
            });
        };

    }
]);

ContactModule.controller('ContactCtrl', [
    '$scope', '$http', '$uibModal', '$routeParams', '$location', function($scope, $http, $uibModal, $routeParams, $location) {

        var templatePrefix = 'Template/Scripts/';

        $scope.title = $routeParams.id == undefined ? "New" : "Edit";
        $http.get('api/Contact/' + ($routeParams.id == undefined ? 0 : $routeParams.id)).success(function(data) {
            $scope.Contact = data;
        });

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[2];


        $scope.openCalendar = function($event) {
            $scope.status.opened = true;
        };

        $scope.status = {
            opened: false
        };

        $scope.go = function(path) {
            $location.path(path);
        };

        $scope.type = function(type) {
            var retValue = "";
            switch (type) {
            case 1:
                retValue = "Phone";
                break;
            case 2:
                retValue = "Mobile";
                break;
            case 3:
                retValue = "Email";
                break;
            case 4:
                retValue = "Fax";
                break;
            case 5:
                retValue = "Skype";
                break;
            default:
                retValue = "Phone";
                break;
            }
            return retValue;
        }

        $scope.addAddress = function() {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: templatePrefix + 'Contact/AddressModal.html',
                controller: 'AddressModalCtrl',
                size: 'lg'
            });

            modalInstance.result.then(function(newAddress) {
                $scope.Contact.Address.push(newAddress);
            });
        };

        $scope.addConnection = function() {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: templatePrefix + 'Contact/ConnectionModal.html',
                controller: 'ConnectionModalCtrl',
                size: 'lg'
            });

            modalInstance.result.then(function(newConnection) {
                $scope.Contact.Connections.push(newConnection);
            });
        };

        $scope.save = function() {
            $http.patch('api/Contact', $scope.Contact).success(function(data) {
                $scope.Contact = data;
            });
        }

        $scope.deleteAddress = function(address) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: templatePrefix + 'Contact/ConfirmationModal.html',
                controller: 'ConfirmationModalCtrl',
                size: 'sm'
            });

            modalInstance.result.then(function(isTrue) {
                if (isTrue) {
                    address.IsDeleted = true;
                }
            });
        };

        $scope.deleteConnection = function(connection) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: templatePrefix + 'Contact/ConfirmationModal.html',
                controller: 'ConfirmationModalCtrl',
                size: 'sm'
            });

            modalInstance.result.then(function(isTrue) {
                if (isTrue) {
                    connection.IsDeleted = true;
                }
            });
        };

    }
]);

