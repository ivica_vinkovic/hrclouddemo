﻿using HRCloudBusinessLayer.DTO;
using System.Collections.Generic;
using HRCloudBusinessLayer;
using HRCloudBusinessLayer.InitialData;

namespace HRCloudDemo.Tests
{
    public class MockData
    {
        public List<Address> Addresses { get; set; }
        public List<Connection> Connections { get; set; }
        public List<Contact> Contacts { get; set; }
        public List<Tags> Tags { get; set; }


        public MockData()
        {
            InitialData InitialData = new InitialData();
            Addresses = InitialData.Addresses;
            Connections = InitialData.Connections;
            Contacts = InitialData.Contacts;
            Tags = InitialData.Tags;
        }
    }
}
