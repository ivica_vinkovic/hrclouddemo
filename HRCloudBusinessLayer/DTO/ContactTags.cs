﻿
namespace HRCloudBusinessLayer.DTO
{
    public class ContactTags
    {
        public int Id { get; set; }
        public int? ContactID { get; set; }
        public int? TagID { get; set; }
        public string Name { get; set; }

        public bool IsDeleted { get; set; }
    }
}
