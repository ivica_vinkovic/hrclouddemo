
namespace HRCloudBusinessLayer.DTO
{
    public class ConnectionType
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
