using System;

namespace HRCloudBusinessLayer.DTO
{
    public class Contact
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Title { get; set; }
        public string MiddleName { get; set; }
        public DateTime? DateOfBirth { get; set; }
    }
}
