﻿using System.Data.Entity.Migrations;
using HRCloudBusinessLayer.EntityFramework;

namespace HRCloudBusinessLayer.DTO.Mapper
{
    public class TagsMapper
    {
		private Tags DtoTags { get; set; }
		private EntityFramework.Tags EfTags { get; set; }

        public TagsMapper(Tags dtoTags)
        {
            DtoTags = dtoTags;
            EfTags = new EntityFramework.Tags();
        }

        public EntityFramework.Tags MapToDatabase()
        {
            using (var db = new TestDBEntities())
            {
                EfTags.Id = DtoTags.Id;
                EfTags.Value = DtoTags.Value;

                db.Set<EntityFramework.Tags>().AddOrUpdate(EfTags);
                db.SaveChanges();
            }

            return EfTags;
        }

        public void DeleteFromDatabase()
        {
            using (var db = new TestDBEntities())
            {
                EfTags.Id = DtoTags.Id;

                db.Tags.Attach(EfTags);
                db.Tags.Remove(EfTags);
                db.SaveChanges();
            }
        }
    }
}
