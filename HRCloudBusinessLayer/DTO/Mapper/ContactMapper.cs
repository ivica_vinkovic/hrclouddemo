﻿using System.Data.Entity.Migrations;
using System.Linq;
using HRCloudBusinessLayer.EntityFramework;

namespace HRCloudBusinessLayer.DTO.Mapper
{
    public class ContactMapper
    {
		private Contact DtoContact { get; set; }
		private EntityFramework.Contact EfContact { get; set; }

        public ContactMapper(Contact dtoContact)
        {
            DtoContact = dtoContact;
            EfContact = new EntityFramework.Contact();
        }

        public EntityFramework.Contact MapToDatabase()
        {
            using (var db = new TestDBEntities())
            {
                EfContact.Id = DtoContact.Id;
                EfContact.Name = DtoContact.Name;
                EfContact.Surname = DtoContact.Surname;
                EfContact.DateOfBirth = DtoContact.DateOfBirth;
                EfContact.MiddleName = DtoContact.MiddleName;
                EfContact.Title = DtoContact.Title;

                db.Set<EntityFramework.Contact>().AddOrUpdate(EfContact);
                db.SaveChanges();
            }

            return EfContact;
        }

        public void DeleteFromDatabase()
        {
            using (var db = new TestDBEntities())
            {
                var contact = (from c in db.Contact
                    where c.Id == DtoContact.Id
                    select c).FirstOrDefault();

                if (contact != null)
                {
                    db.Address.RemoveRange(contact.Address);
                    db.Connection.RemoveRange(contact.Connection);
                    db.ContactTags.RemoveRange(contact.ContactTags);
                    db.Contact.Attach(contact);
                    db.Contact.Remove(contact);
                }
                db.SaveChanges();
            }
        }
    }
}
