﻿using System.Data.Entity.Migrations;
using HRCloudBusinessLayer.EntityFramework;

namespace HRCloudBusinessLayer.DTO.Mapper
{
    public class AddressMapper
    {
		private Address DtoAddress { get; set; }
		private EntityFramework.Address EfAddress { get; set; }

        public AddressMapper(Address dtoAddress)
        {
            DtoAddress = dtoAddress;
            EfAddress = new EntityFramework.Address();
        }

        public EntityFramework.Address MapToDatabase()
        {
            using (var db = new TestDBEntities())
            {
                EfAddress.Id = DtoAddress.Id;
                EfAddress.City = DtoAddress.City;
                EfAddress.ContactID = DtoAddress.ContactID;
                EfAddress.Country = DtoAddress.Country;
                EfAddress.County = DtoAddress.County;
                EfAddress.No = DtoAddress.No;
                EfAddress.Street = DtoAddress.Street;
                EfAddress.ZipCode = DtoAddress.ZipCode;

                db.Set<EntityFramework.Address>().AddOrUpdate(EfAddress);
                db.SaveChanges();
            }

            return EfAddress;
        }

        public void DeleteFromDatabase()
        {
            using (var db = new TestDBEntities())
            {
                EfAddress.Id = DtoAddress.Id;

                db.Address.Attach(EfAddress);
                db.Address.Remove(EfAddress);
                db.SaveChanges();
            }
        }
    }
}
