﻿using System.Data.Entity.Migrations;
using HRCloudBusinessLayer.EntityFramework;

namespace HRCloudBusinessLayer.DTO.Mapper
{
    public class ContactTagsMapper
    {
		private ContactTags DtoContactTags { get; set; }
		private EntityFramework.ContactTags EfContactTags { get; set; }

        public ContactTagsMapper(ContactTags dtoContactTags)
        {
            DtoContactTags = dtoContactTags;
            EfContactTags = new EntityFramework.ContactTags();
        }

        public EntityFramework.ContactTags MapToDatabase()
        {
            using (var db = new TestDBEntities())
            {
                EfContactTags.Id = DtoContactTags.Id;
                EfContactTags.ContactID = DtoContactTags.ContactID;
                EfContactTags.TagID = DtoContactTags.TagID;

                db.Set<EntityFramework.ContactTags>().AddOrUpdate(EfContactTags);
                db.SaveChanges();
            }

            return EfContactTags;
        }

        public void DeleteFromDatabase()
        {
            using (var db = new TestDBEntities())
            {
                EfContactTags.Id = DtoContactTags.Id;

                db.ContactTags.Attach(EfContactTags);
                db.ContactTags.Remove(EfContactTags);
                db.SaveChanges();
            }
        }
    }
}
