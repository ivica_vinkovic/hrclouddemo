﻿using System.Data.Entity.Migrations;
using HRCloudBusinessLayer.EntityFramework;

namespace HRCloudBusinessLayer.DTO.Mapper
{
    public class ConnectionTypeMapper
    {
		private ConnectionType DtoConnectionType { get; set; }
		private EntityFramework.ConnectionType EfConnectionType { get; set; }

        public ConnectionTypeMapper(ConnectionType dtoConnectionType)
        {
            DtoConnectionType = dtoConnectionType;
            EfConnectionType = new EntityFramework.ConnectionType();
        }

        public EntityFramework.ConnectionType MapToDatabase()
        {
            using (var db = new TestDBEntities())
            {
                EfConnectionType.Id = DtoConnectionType.Id;
                EfConnectionType.Type = DtoConnectionType.Type;

                db.Set<EntityFramework.ConnectionType>().AddOrUpdate(EfConnectionType);
                db.SaveChanges();
            }

            return EfConnectionType;
        }

        public void DeleteFromDatabase()
        {
            using (var db = new TestDBEntities())
            {
                EfConnectionType.Id = DtoConnectionType.Id;

                db.ConnectionType.Attach(EfConnectionType);
                db.ConnectionType.Remove(EfConnectionType);
                db.SaveChanges();
            }
        }
    }
}
