﻿using System.Data.Entity.Migrations;
using HRCloudBusinessLayer.EntityFramework;

namespace HRCloudBusinessLayer.DTO.Mapper
{
    public class ConnectionMapper
    {
		private Connection DtoConnection { get; set; }
		private EntityFramework.Connection EfConnection { get; set; }

        public ConnectionMapper(Connection dtoConnection)
        {
            DtoConnection = dtoConnection;
            EfConnection = new EntityFramework.Connection();
        }

        public EntityFramework.Connection MapToDatabase()
        {
            using (var db = new TestDBEntities())
            {
                EfConnection.Id = DtoConnection.Id;
                EfConnection.ContactID = DtoConnection.ContactID;
                EfConnection.TypeID = DtoConnection.TypeID;
                EfConnection.Value = DtoConnection.Value;


                db.Set<EntityFramework.Connection>().AddOrUpdate(EfConnection);
                db.SaveChanges();
            }

            return EfConnection;
        }

        public void DeleteFromDatabase()
        {
            using (var db = new TestDBEntities())
            {
                EfConnection.Id = DtoConnection.Id;

                db.Connection.Attach(EfConnection);
                db.Connection.Remove(EfConnection);
                db.SaveChanges();
            }
        }
    }
}
