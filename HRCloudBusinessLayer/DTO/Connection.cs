
namespace HRCloudBusinessLayer.DTO
{
    public class Connection
    {
        public int Id { get; set; }
        public int? TypeID { get; set; }
        public string Value { get; set; }
        public int? ContactID { get; set; }

        public bool IsDeleted { get; set; }
    }
}
