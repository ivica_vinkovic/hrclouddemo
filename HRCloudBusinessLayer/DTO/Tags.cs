
namespace HRCloudBusinessLayer.DTO
{
    public class Tags
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public bool IsDeleted { get; set; }
    }
}
