
namespace HRCloudBusinessLayer.DTO
{
    public class Address
    {
        public int Id { get; set; }
        public string Street { get; set; }
        public string No { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public int? ContactID { get; set; }

        public bool IsDeleted { get; set; }
    }
}
