﻿using System;
using System.Collections.Generic;
using System.Linq;
using HRCloudBusinessLayer.DTO.Mapper;
using HRCloudBusinessLayer.EntityFramework;
using Address = HRCloudBusinessLayer.DTO.Address;
using Connection = HRCloudBusinessLayer.DTO.Connection;
using Contact = HRCloudBusinessLayer.DTO.Contact;
using ContactTags = HRCloudBusinessLayer.DTO.ContactTags;
using Tags = HRCloudBusinessLayer.DTO.Tags;

namespace HRCloudBusinessLayer.InitialData
{
    public class InitialData
    {
        public List<Address> Addresses { get; set; }
        public List<Connection> Connections { get; set; }
        public List<Contact> Contacts { get; set; }
        public List<Tags> Tags { get; set; }
        public List<ContactTags> ContactsTags { get; set; }

        public InitialData()
        {
            Addresses = new List<Address>();
            GenerateAddresses();
            Connections = new List<Connection>();
            GenerateConnections();
            Tags = new List<Tags>();
            GenerateTags();
            Contacts = new List<Contact>();
            GenerateContacts();
            ContactsTags = new List<ContactTags>();
            GenerateContactsTags();
        }

        public void Fill()
        {
            using (var db = new TestDBEntities())
            {
                var contacts = (from c in db.Contact
                    select new Contact()
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Surname = c.Surname,
                        Title = c.Title,
                        MiddleName = c.MiddleName,
                        DateOfBirth = c.DateOfBirth
                    }).ToList();
                if (contacts.Any())
                {
                    return;
                }
            }

            foreach (var contact in Contacts)
            {
                int contactId = contact.Id;
                ContactMapper mapper = new ContactMapper(contact);
                var newContact = mapper.MapToDatabase();
                Addresses.Where(w => w.ContactID == contactId)
                    .ToList()
                    .ForEach(x => { x.ContactID = newContact.Id; });
                Connections.Where(w => w.ContactID == contactId)
                    .ToList()
                    .ForEach(x => { x.ContactID = newContact.Id; });
                ContactsTags.Where(w => w.ContactID == contactId)
                    .ToList()
                    .ForEach(x => { x.ContactID = newContact.Id; });
            }

            foreach (var tag in Tags)
            {
                TagsMapper mapper = new TagsMapper(tag);
                mapper.MapToDatabase();
            }

            foreach (var address in Addresses)
            {
                AddressMapper mapper = new AddressMapper(address);
                mapper.MapToDatabase();
            }

            foreach (var connection in Connections)
            {
                ConnectionMapper mapper = new ConnectionMapper(connection);
                mapper.MapToDatabase();
            }

            foreach (var contactTag in ContactsTags)
            {
                ContactTagsMapper mapper = new ContactTagsMapper(contactTag);
                mapper.MapToDatabase();
            }
        }

        public void GenerateAddresses()
        {
            Address newAddress = new Address()
            {
                City = "West Madilyn",
                No = "287",
                Country = "South Korea",
                ZipCode = "52878",
                Street = "Mann Ports",
                Id = 10,
                ContactID = 4
            };
            Addresses.Add(newAddress);
            Address newAddress2 = new Address()
            {
                City = "Hoegertown",
                No = "35",
                Country = "Hong Kong SAR China",
                ZipCode = "10971",
                Street = "Daisy Brook",
                Id = 1,
                ContactID = 1
            };
            Addresses.Add(newAddress2);
            Address newAddress3 = new Address()
            {
                City = "North Thelma",
                No = "121",
                Country = "Palau",
                ZipCode = "44757-3086",
                Street = "Beier Harbors",
                Id = 2,
                ContactID = 2
            };
            Addresses.Add(newAddress3);
            Address newAddress4 = new Address()
            {
                City = "Ullrichside",
                No = "897",
                Country = "Niger",
                ZipCode = "86623",
                Street = "Astrid Freeway",
                Id = 3,
                ContactID = 4
            };
            Addresses.Add(newAddress4);
            Address newAddress5 = new Address()
            {
                City = "Predovictown",
                No = "374",
                Country = "Georgia",
                ZipCode = "49674-5417",
                Street = "Malika Trail",
                Id = 4,
                ContactID = 3
            };
            Addresses.Add(newAddress5);
            Address newAddress6 = new Address()
            {
                City = "South Darrellside",
                No = "246",
                Country = "Kuwait",
                ZipCode = "73932-0811",
                Street = "Simonis Drive",
                Id = 5,
                ContactID = 5
            };
            Addresses.Add(newAddress6);
            Address newAddress7 = new Address()
            {
                City = "Kirstinside",
                No = "896",
                Country = "Lithuania",
                ZipCode = "56949-1342",
                Street = "Sven Junctions",
                Id = 6,
                ContactID = 7
            };
            Addresses.Add(newAddress7);
            Address newAddress8 = new Address()
            {
                City = "Gerholdmouth",
                No = "13",
                Country = "Guadeloupe",
                ZipCode = "38217-0800",
                Street = "Shanahan Points",
                Id = 7,
                ContactID = 8
            };
            Addresses.Add(newAddress8);
            Address newAddress9 = new Address()
            {
                City = "Port Shakiraburgh",
                No = "605",
                Country = "Moldova",
                ZipCode = "68360-3914",
                Street = "Susan Crest",
                Id = 8,
                ContactID = 6
            };
            Addresses.Add(newAddress9);
            Address newAddress10 = new Address()
            {
                City = "Haagville",
                No = "350",
                Country = "Ethiopia",
                ZipCode = "27852-9957",
                Street = "Thompson Vista",
                Id = 9,
                ContactID = 3
            };
            Addresses.Add(newAddress10);
        }

        public void GenerateConnections()
        {
            Connection connection = new Connection()
            {
                TypeID = 3,
                Id = 8,
                Value = "Damion@georgette.me",
                ContactID = 6
            };
            Connections.Add(connection);
            Connection connection1 = new Connection()
            {
                TypeID = 3,
                Id = 1,
                Value = "Serena.Hoeger@alexandria.biz",
                ContactID = 3
            };
            Connections.Add(connection1);
            Connection connection2 = new Connection()
            {
                TypeID = 3,
                Id = 2,
                Value = "Madison_Hettinger@seth.tv",
                ContactID = 1
            };
            Connections.Add(connection2);
            Connection connection3 = new Connection()
            {
                TypeID = 3,
                Id = 3,
                Value = "Lonzo_Boehm@terrance.me",
                ContactID = 2
            };
            Connections.Add(connection3);
            Connection connection4 = new Connection()
            {
                TypeID = 3,
                Id = 4,
                Value = "Lia_Vandervort@marina.tv",
                ContactID = 4
            };
            Connections.Add(connection4);
            Connection connection5 = new Connection()
            {
                TypeID = 3,
                Id = 5,
                Value = "Nova_Ryan@onie.tv",
                ContactID = 7
            };
            Connections.Add(connection5);
            Connection connection6 = new Connection()
            {
                TypeID = 3,
                Id = 6,
                Value = "Brad@zoie.tv",
                ContactID = 5
            };
            Connections.Add(connection6);
            Connection connection7 = new Connection()
            {
                TypeID = 3,
                Id = 7,
                Value = "Conrad_Boyle@theo.info",
                ContactID = 5
            };
            Connections.Add(connection7);
            Connection connection8 = new Connection()
            {
                TypeID = 1,
                Id = 9,
                Value = "823.004.5271",
                ContactID = 2
            };
            Connections.Add(connection8);
            Connection connection9 = new Connection()
            {
                TypeID = 1,
                Id = 10,
                Value = "(853)983-5166",
                ContactID = 7
            };
            Connections.Add(connection9);
            Connection connection10 = new Connection()
            {
                TypeID = 2,
                Id = 12,
                Value = "(379)447-0845 x9691",
                ContactID = 8
            };
            Connections.Add(connection10);
            Connection connection11 = new Connection()
            {
                TypeID = 1,
                Id = 13,
                Value = "469-437-3410",
                ContactID = 8
            };
            Connections.Add(connection11);
            Connection connection12 = new Connection()
            {
                TypeID = 2,
                Id = 11,
                Value = "(389)873-3908",
                ContactID = 1
            };
            Connections.Add(connection12);
        }

        public void GenerateContacts()
        {
            Contact contact = new Contact()
            {
                Surname = "Cassin",
                Name = "Leonor",
                Title = "",
                MiddleName = "Kristin",
                DateOfBirth = new DateTime(1980, 7, 18),
                Id = 1
            };
            Contacts.Add(contact);
            Contact contact2 = new Contact()
            {
                Surname = "Batz",
                Name = "Shaniya",
                Title = "",
                MiddleName = "",
                DateOfBirth = new DateTime(1980, 5, 27),
                Id = 2
            };
            Contacts.Add(contact2);
            Contact contact3 = new Contact()
            {
                Surname = "Terry",
                Name = "Ryan",
                Title = "Phd.",
                MiddleName = "Mauricio",
                DateOfBirth = new DateTime(1960, 7, 29),
                Id = 3
            };
            Contacts.Add(contact3);
            Contact contact4 = new Contact()
            {
                Surname = "Emard",
                Name = "Harley",
                Title = "Mr.",
                MiddleName = "Manuel",
                DateOfBirth = new DateTime(1958, 12, 18),
                Id = 4
            };
            Contacts.Add(contact4);
            Contact contact5 = new Contact()
            {
                Surname = "Koelpin",
                Name = "Aglae",
                Title = "Mrs.",
                MiddleName = "Amina",
                DateOfBirth = new DateTime(1995, 4, 26),
                Id = 5
            };
            Contacts.Add(contact5);
            Contact contact6 = new Contact()
            {
                Surname = "Ernser",
                Name = "Cali",
                Title = "Md.",
                MiddleName = "Karlie",
                DateOfBirth = new DateTime(1991, 8, 27),
                Id = 6
            };
            Contacts.Add(contact6);
            Contact contact7 = new Contact()
            {
                Surname = "McLaughlin",
                Name = "Jordon",
                Title = "Ms.",
                MiddleName = "",
                DateOfBirth = new DateTime(1986, 7, 18),
                Id = 7
            };
            Contacts.Add(contact7);
            Contact contact8 = new Contact()
            {
                Surname = "Dicki",
                Name = "Estella",
                Title = "Mrs.",
                MiddleName = "Karli",
                DateOfBirth = new DateTime(1945, 7, 18),
                Id = 8
            };
            Contacts.Add(contact8);
        }

        public void GenerateTags()
        {
            Tags tags = new Tags()
            {
                Id = 1,
                Value = "Buyer"
            };

            Tags.Add(tags);

            Tags tags2 = new Tags()
            {
                Id = 2,
                Value = "Seller"
            };

            Tags.Add(tags2);

            Tags tags3 = new Tags()
            {
                Id = 3,
                Value = "Washer"
            };

            Tags.Add(tags3);

            Tags tags4 = new Tags()
            {
                Id = 4,
                Value = "Valuable customer"
            };

            Tags.Add(tags4);

            Tags tags5 = new Tags()
            {
                Id = 5,
                Value = "Travel agent"
            };

            Tags.Add(tags5);
        }

        public void GenerateContactsTags()
        {
            ContactTags contactTags = new ContactTags()
            {
                ContactID = 1,
                TagID = 1
            };

            ContactsTags.Add(contactTags);

            ContactTags contactTags2 = new ContactTags()
            {
                ContactID = 2,
                TagID = 1
            };

            ContactsTags.Add(contactTags2);

            ContactTags contactTags3 = new ContactTags()
            {
                ContactID = 3,
                TagID = 1
            };

            ContactsTags.Add(contactTags3);

            ContactTags contactTags4 = new ContactTags()
            {
                ContactID = 3,
                TagID = 2
            };

            ContactsTags.Add(contactTags4);

            ContactTags contactTags5 = new ContactTags()
            {
                ContactID = 3,
                TagID = 4
            };

            ContactsTags.Add(contactTags5);

            ContactTags contactTags6 = new ContactTags()
            {
                ContactID = 4,
                TagID = 4
            };

            ContactsTags.Add(contactTags6);

            ContactTags contactTags7 = new ContactTags()
            {
                ContactID = 5,
                TagID = 4
            };

            ContactsTags.Add(contactTags7);

            ContactTags contactTags8 = new ContactTags()
            {
                ContactID = 6,
                TagID = 4
            };

            ContactsTags.Add(contactTags8);

            ContactTags contactTags9 = new ContactTags()
            {
                ContactID = 6,
                TagID = 1
            };

            ContactsTags.Add(contactTags9);

            ContactTags contactTags10 = new ContactTags()
            {
                ContactID = 8,
                TagID = 5
            };

            ContactsTags.Add(contactTags10);
        }
    }
}
