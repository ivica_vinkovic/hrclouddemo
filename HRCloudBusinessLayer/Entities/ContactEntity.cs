﻿using System.Collections.Generic;
using System.Linq;
using HRCloudBusinessLayer.DTO.Mapper;
using HRCloudBusinessLayer.EntityFramework;
using Address = HRCloudBusinessLayer.DTO.Address;
using Connection = HRCloudBusinessLayer.DTO.Connection;
using Contact = HRCloudBusinessLayer.DTO.Contact;
using ContactTags = HRCloudBusinessLayer.DTO.ContactTags;

namespace HRCloudBusinessLayer.Entities
{
    public class ContactEntity
    {
        public List<Address> Address { get; set; }
        public List<ContactTags> Tags;
        public List<Connection> Connections;
        public Contact Contact { get; set; }

        public ContactEntity(int id)
        {
            if (id == 0)
            {
                Address = new List<Address>();
                var address = new Address();
                Address.Add(address);
                Tags = new List<ContactTags>();
                Connections = new List<Connection>();
                var connection = new Connection();
                Connections.Add(connection);
                Contact = new Contact();
                return;
            }
            using (var db = new TestDBEntities())
            {
                Contact = (from c in db.Contact
                    where c.Id.Equals(id)
                    select new Contact()
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Surname = c.Surname,
                        Title = c.Title,
                        MiddleName = c.MiddleName,
                        DateOfBirth = c.DateOfBirth
                    }).FirstOrDefault();

                Address = (from a in db.Address
                    where a.ContactID == id
                    select new Address()
                    {
                        Id = a.Id,
                        Street = a.Street,
                        No = a.No,
                        ZipCode = a.ZipCode,
                        City = a.City,
                        County = a.County,
                        Country = a.Country,
                        ContactID = a.ContactID
                    }).ToList();

                Tags = (from ct in db.ContactTags
                            join t in db.Tags on ct.TagID equals t.Id 
                        where ct.ContactID == id
                        select new ContactTags()
                        {
                            Id = ct.Id,
                            TagID = ct.TagID,
                            ContactID = ct.ContactID,
                            Name = t.Value
                        }).ToList();

                Connections = (from c in db.Connection
                    where c.ContactID == id
                    select new Connection()
                    {
                        Id = c.Id,
                        TypeID = c.TypeID,
                        Value = c.Value,
                        ContactID = c.ContactID
                    }).ToList();
            }
        }

        public ContactEntity UpSert()
        {
            ContactMapper contactMapper = new ContactMapper(Contact);
            Contact.Id = contactMapper.MapToDatabase().Id;
            Address.ForEach(x => { x.ContactID = Contact.Id; });
            Tags.ForEach(x => { x.ContactID = Contact.Id; });
            Connections.ForEach(x => { x.ContactID = Contact.Id; });

            foreach (var address in Address)
            {
                AddressMapper mapper = new AddressMapper(address);
                if (address.IsDeleted)
                {
                    mapper.DeleteFromDatabase();
                }
                else
                {
                    address.Id = mapper.MapToDatabase().Id;
                }
            }

            foreach (var connection in Connections)
            {
                ConnectionMapper mapper = new ConnectionMapper(connection);
                if (connection.IsDeleted)
                {
                    mapper.DeleteFromDatabase();
                }
                else
                {
                    connection.Id = mapper.MapToDatabase().Id;
                }
            }

            foreach (var contactTagse in Tags)
            {
                ContactTagsMapper mapper = new ContactTagsMapper(contactTagse);
                if (contactTagse.IsDeleted)
                {
                    mapper.DeleteFromDatabase();
                }
                else
                {
                    contactTagse.Id = mapper.MapToDatabase().Id;
                }
            }

            Address = Address.Where(x => !x.IsDeleted).ToList();
            Connections = Connections.Where(x => !x.IsDeleted).ToList();
            Tags = Tags.Where(x => !x.IsDeleted).ToList();

            return this;
        }
    }
}
