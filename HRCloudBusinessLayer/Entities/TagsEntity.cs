﻿using System.Collections.Generic;
using System.Linq;
using HRCloudBusinessLayer.DTO.Mapper;
using HRCloudBusinessLayer.EntityFramework;
using Tags = HRCloudBusinessLayer.DTO.Tags;

namespace HRCloudBusinessLayer.Entities
{
    public class TagsEntity
    {
        public List<Tags> Tag { get; set; }

        public void Fill()
        {
            using (var db = new TestDBEntities())
            {
                Tag = (from ct in db.Tags
                    select new Tags()
                    {
                        Id = ct.Id,
                        Value = ct.Value
                    }).ToList();
            }
        }

        public TagsEntity()
        {
            
        }

        public List<Tags> UpSert()
        {
            foreach (var tag in Tag)
            {
                TagsMapper mapper = new TagsMapper(tag);
                if (tag.IsDeleted)
                {
                    mapper.DeleteFromDatabase();
                }
                else
                {
                    tag.Id = mapper.MapToDatabase().Id;
                }
            }

            Tag = Tag.Where(x => !x.IsDeleted).ToList();

            return Tag;
        }
    }
}
