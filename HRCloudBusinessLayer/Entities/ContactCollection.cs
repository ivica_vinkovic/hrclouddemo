﻿using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.Serialization;
using HRCloudBusinessLayer.EntityFramework;

namespace HRCloudBusinessLayer.Entities
{
    [DataContract]
    public class ContactCollection
    {
        [DataMember]
        public List<ContactEntity> Contacts { get; set; }

        public List<int> Ids { get; set; }

        public ContactCollection(string searchContact)
        {
            using (var db = new TestDBEntities())
            {
                Ids = (from c in db.Contact
                       join ct in db.ContactTags on c.Id equals ct.ContactID into lj
                       from gj in lj.DefaultIfEmpty()
                       join t in db.Tags on gj.TagID equals t.Id into tlj
                       from tl in tlj.DefaultIfEmpty()
                       where (c.Name.Contains(searchContact) || c.Surname.Contains(searchContact) || tl.Value.Contains(searchContact))
                       select c.Id).ToList();

            }
            Contacts = new List<ContactEntity>(Ids.Distinct().Count());
            foreach (var id in Ids.Distinct())
            {
                Contacts.Add(new ContactEntity(id));
            }
        }
    }

}
