﻿using System.Collections.Generic;
using System.Linq;
using HRCloudBusinessLayer.EntityFramework;
using ConnectionType = HRCloudBusinessLayer.DTO.ConnectionType;

namespace HRCloudBusinessLayer.Entities
{
    internal class ConnectionTypes
    {
        public List<ConnectionType> ConnectionType { get; set; }

        public ConnectionTypes()
        {
            using (var db = new TestDBEntities())
            {
                ConnectionType = (from ct in db.ConnectionType
                    select new ConnectionType()
                    {
                        Id = ct.Id,
                        Type = ct.Type
                    }).ToList();
            }
        }
    }
}
